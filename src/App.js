import './App.css';
import React, { useEffect, useState } from 'react'

import Recipe from './components/Recipe/Recipe'


function App() {

  // VARIABLES
  const APP_ID = 'xxx';
  const APP_KEY = 'xxx';

  // USE STATE
  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState('');
  const [query, setQuery] = useState('tofu');

  // USE EFFECT
  useEffect(() => {
    getRecipes();
  }, [query])

  // FUNCTIONS
  const getRecipes = async () => {
    const response = await fetch(
      `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`
    );
    const data = await response.json();
    setRecipes(data.hits);
  }

  const updateSearch = (event) => {
    setSearch(event.target.value);
  }

  const getSearch = (event) => {
    event.preventDefault();
    setQuery(search);
    setSearch('');
  }

  return (
    <div className="App">
      <h1>Recipe App</h1>
      <form onSubmit={getSearch} className="search-form">

        <input 
          className="search-bar" 
          type="text" 
          value={search}
          onChange={updateSearch}
        />

        <button className="search-btn" type="submit">
          Search
        </button>
      </form>

      <div className="recipe-container">
        {recipes.map(recipe =>(
          <Recipe 
            title={recipe.recipe.label} 
            calories={recipe.recipe.calories} 
            img={recipe.recipe.image}
            key={recipe.recipe.label}
          />
        ))}
      </div>

    </div>
  );
}

export default App;
