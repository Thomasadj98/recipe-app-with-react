import React from 'react'

const Recipe = ({ title, calories, img }) => {
    return(
        <div className="grid-item">
            <div className="recipe-image-container">
                <img className="recipe-img" src={img} alt={title} />
            </div>
            <h1>{title}</h1>
            <p>{calories}</p>
        </div>
    )
}

export default Recipe